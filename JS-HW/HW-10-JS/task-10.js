/*
Задание
Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для двух полей ввода пароля.
По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения

После нажатия на кнопку страница не должна перезагружаться
Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
 */

function showPassword(eve){
    if(eve.target.classList.contains('fa-eye')){
        eve.target.classList.replace("fa-eye","fa-eye-slash")
        eve.target.parentElement.querySelector('input').setAttribute("type", "text")
    }
    else{
        eve.target.classList.replace("fa-eye-slash", "fa-eye")
        eve.target.parentElement.querySelector('input').setAttribute("type", "password")
    }
    return false
}

let mainForm = document.querySelector(".password-form")
mainForm.addEventListener('click', (event) => {
    if (event.target.classList.contains('icon-password')){
        showPassword(event)
    }
})

let sendBut = document.body.querySelector(".btn")
sendBut.insertAdjacentHTML("beforebegin", "<p class='error'></p>")
let error = document.querySelector(".error")
sendBut.addEventListener('click', (event) => {
    let fields = [...document.querySelectorAll("input")]
    event.preventDefault()
    if(fields.reduce((first,second) => first.value === second.value) && fields.every(elem => elem.value !== "")){
        error.innerText = ""
        alert("You are welcome")
    }
    else{
        error.innerText = "Нужно ввести одинаковые значения"
        document.querySelector("p").style.color = "red"
    }
})
let field = document.getElementById("main-field")
field.addEventListener('focus', () => {
    document.querySelector("p").remove()
})
