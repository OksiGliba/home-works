
let images = document.querySelectorAll(".image-to-show")
let currentImage = 0
let timer = setInterval(showImages, 3000)

function showImages() {
    if (images[currentImage].classList.contains('image-to-show')) {
        images[currentImage].className = "active"
    } else if (images[currentImage].classList.contains("active")) {
        images[currentImage].className = "image-to-show"
        currentImage = ++currentImage % images.length
        images[currentImage].className = "active"
    }
}

let imageBlock = document.querySelector(".images-wrapper")
if (document.querySelector(".btns-block") === null) {
    imageBlock.insertAdjacentHTML("afterend", `
        <div class="btns-block">
            <button class="stop-btn">Прекратить</button>
            <button class="run-btn">Возобновить показ</button>
        </div>`)
}
document.querySelector(".stop-btn").addEventListener("click", () => {
    clearInterval(timer)
})
document.querySelector(".run-btn").addEventListener("click", () => {
    if (timer) {
        clearInterval(timer)
    }
    timer = setInterval(showImages, 3000)
})
