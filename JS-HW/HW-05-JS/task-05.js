/*
Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
Создать метод getAge() который будет возвращать сколько пользователю лет.
Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
 */


const createNewUser = function () {
    let firstName = prompt("Enter your name")
    let lastName = prompt("Enter your last name")
    let birthday = prompt("Enter your date of birth dd.mm.yyyy")
    let newUser = {
        firstName,
        lastName,
        birthday,

        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },

        getAge:function(){
            let currentYear=new Date().getFullYear()
            return (currentYear-this.birthday.slice(-4))
       },
        getPassword:function(){
            return (this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.slice(-4))
        },
    }
    newUser.login = newUser.getLogin()
    newUser.password = newUser.getPassword()
    newUser.age=newUser.getAge()
     return newUser;
}

let newUser = createNewUser()

console.log(newUser)
console.log(newUser.getLogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())




/*
Теоретический вопрос


Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

Экранирвание - это специальные символы в программировании (/ или \). Например, для поиска специальных символов [ \ ^ $ . | ? * + ( ), нам нужно добавить перед ними \ («экранировать их»). Экранирование используется для сокращенной записи кода, а в общем - для удобства чтения кода.
 */
