/*



Задание
Реализовать функцию подсветки нажимаемых клавиш. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

В файле index.html лежит разметка для кнопок.
Каждая кнопка содержит в себе название клавиши на клавиатуре
По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.
 */
let keyBtns = document.querySelectorAll('.btn')
document.body.addEventListener('keypress', (event)=>{
let activeBtn = document.querySelector('.active-btn')
if(activeBtn)
    {
        activeBtn.classList.remove('active-btn')
    }
    keyBtns=[...keyBtns]
    keyBtns.map(btn => {
       
        if(event.key === btn.innerText.toLowerCase() || event.code === btn.innerText)
        {
            btn.classList.add('active-btn')
        }
        }).join()
        
    })




