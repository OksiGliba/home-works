document.body.insertAdjacentHTML('afterbegin', '<div id = "parent-box"><input id = "parentInput" type="text" placeholder = "Price"></input></div>')

function focus() {
    let getElement = document.getElementById('parentInput')
    getElement.onfocus = function () {
        getElement.classList.toggle('parent-input-green')
    }

    getElement.onblur = function () {
        let checker = document.getElementById('curentPrice')
        getElement.classList.toggle('parent-input-green')
        let getParent = document.getElementById('parent-box')
        if (getElement.value > null) {
            getElement.style.color = 'green'
            if(checker === null){
            getParent.insertAdjacentHTML('afterbegin', `<span id="curentPrice">Current price: ${getElement.value}<button id = "cancelPrice">x</button></span>`)
        }else checker.innerHTML = `Current price: ${getElement.value}<button id = "cancelPrice">x</button>`
            let cancelPrice = document.getElementById('cancelPrice')
            cancelPrice.addEventListener('click', () => cancelPrice.parentNode.remove())
            
            function remover() {
                let removeWarning = document.getElementById('warning')
                if (removeWarning !== null) {
                    removeWarning.parentNode.removeChild(removeWarning)
                    getElement.classList.remove('parent-input-red')
                }
                
            }
            remover()
        } else {
            
            let warning = document.getElementById('warning')
            
            if (warning === null && getElement.value !== '') {
                getElement.style.color = 'red'
                getElement.classList.add('parent-input-red')
                getParent.insertAdjacentHTML('beforeend', `<span id="warning">Please enter correct price</span>`)
            }
            return
        }

    }
}

focus()